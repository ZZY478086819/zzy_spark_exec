package utils;

import org.spark_project.dmg.pmml.True;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.io.File;

public class product {
    static final String Province = "北京市，天津市，上海市，重庆市，" +
            "河北省，山西省，辽宁省，吉林省，黑龙江省，江苏省，" +
            "浙江省，安徽省，福建省，江西省，山东省，河南省，湖北省，" +
            "湖南省，广东省，海南省，四川省，贵州省，云南省，陕西省，甘肃省，青海省，台湾省";
    static String Province_list[] = null;
    static char char_num[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    static Random ran = new Random();
    static final String phone_prefix[] = {"130", "131", "132", "155", "156", "186", "185",
            "133", "153", "180", "181", "189",
            "152", "157", "158", "159", "182", "183", "188", "187"};
    static final String URL[] = {"www.dangdang.com", "www.baidu.com", "www.suning.com", "www.mi.com", "www.taobao.com"};
    static final String operation[] = {"Login", "Logout", "Leave"};

    static {
        Province_list = Province.split("，");
    }

    public static void main(String[] args) {

        File file = new File("file_collect/pv.log");
        if (!file.exists()) {
            try {
                file.createNewFile();
                write_file(file);
            } catch (IOException e) {
                System.out.println("文件创建失败！");
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    private static void write_file(File file) {
        FileWriter fr = null;
        BufferedWriter bw=null;
        try {
            file.createNewFile();
            fr = new FileWriter(file,true);
            bw=new BufferedWriter(fr);
            for (int i=0;i<1000;i++) {
                //7.213.213.208    吉林    2018-03-29    1522294977303    1920936170939152672    www.dangdang.com    Login
                String list[] = {create_ip(), create_Province(),
                        create_date(), create_phone(), create_id(), create_url()
                        , create_operation()};
                String line = create_line(list);
                bw.write(line);
                bw.newLine();
            }
        } catch (IOException e) {
            System.out.println("文件创建失败！");
            e.printStackTrace();
            System.exit(1);
        } finally {
            if (fr != null) {
                try {
                    bw.flush();
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String create_ip() {
        String ip = "";
        int num_first = ran.nextInt(255 + 1);
        while(num_first==0){
            num_first = ran.nextInt(255 + 1);
        }
        ip = num_first + ip;
        for (int i = 0; i < 3; i++) {
            int num = ran.nextInt(255 + 1);
            ip = ip + "." + num;
        }
        return ip;
    }

    private static String create_Province() {
        return Province_list[ran.nextInt(Province_list.length - 1)];
    }

    private static String create_date() {
        int year = ran.nextInt(20) + 2000;  //生成[2000,2019]的整数；年
        int month = ran.nextInt(12) + 1;   //生成[1,12]的整数；月
        int Day = ran.nextInt(30) + 1;       //生成[1,30)的整数；日
        return year + "-" + month + "-" + Day;
    }

    private static String create_phone() {
        String phone = phone_prefix[ran.nextInt(phone_prefix.length - 1)];
        for (int i = 0; i < 10; i++) {
            char num_c = char_num[ran.nextInt(char_num.length - 1)];
            phone = phone + num_c;
        }
        return phone;
    }

    private static String create_id() {
        String id = "";
        for (int i = 0; i < 19; i++) {
            id = id + char_num[ran.nextInt(char_num.length - 1)];
        }
        return id;
    }

    private static String create_url() {
        return URL[ran.nextInt(URL.length - 1)];
    }

    private static String create_operation() {
        return operation[ran.nextInt(operation.length - 1)];
    }

    private static String create_line(String ele[]) {
        StringBuilder sb = new StringBuilder();
        for (String col : ele) {
            sb.append(col).append("\t");
        }
        return sb.substring(0, sb.length() - 1).toString();
    }
}
