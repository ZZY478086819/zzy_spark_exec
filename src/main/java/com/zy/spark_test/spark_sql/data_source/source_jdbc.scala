package com.zy.spark_test.spark_sql.data_source

import java.util.Properties

import org.apache.spark.sql.{DataFrame, SparkSession}

object source_jdbc {
  def main(args: Array[String]): Unit = {
    /**
      * spark 处理MySQL数据库中的数据：
      * 1. 需要在spark类路径中包含特定数据库的JDBC驱动程序
      */
    val spark = SparkSession.builder()
      .appName("source_hive")
      .master("local[*]")
      .getOrCreate()

    //读取jdbc的数据

    //1.option方式设置参数
    var jdbcDF1: DataFrame = spark.read.format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option("dbtable", "schema.tablename")
      .option("user", "username")
      .option("password", "user_password")
      .load()

    //.2Properties方式设置
    val connectionProperties = new Properties()
    connectionProperties.put("user","username")
    connectionProperties.put("password","password")
    val url="jdbc:postgresql:dbserver"
    val tablename="tablename"
    var jdbcDF2=spark.read.jdbc(url,tablename,connectionProperties)


    //写入jdbc的数据
    jdbcDF1.write.format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
        .option("dbtable", "schema.tablename")
      .option("user", "username")
      .option("password", "password")
      .save()

    jdbcDF2.write.jdbc(url,tablename,connectionProperties)

    /**
      * 这里介绍一下option中的一些配置：
      * 1.url：连接数据库的URL
      * 2.dbtable：表名
      * 3.query：加载的数据源，用select的方式在数据库查询之后返回成DataFrame
      * 4.driver：JDBC的驱动，以MySQL为例就是：com.mysql.jdbc.Driver
      * 5.partitionColumn, lowerBound, upperBound：它们描述了如何在从多个worker并行读取数据时对表进行分区。
      *   partitionColumn：必须是表中的数字、日期或时间戳列
      *   lowerBound、upperBound：下界和上界只是用来决定分区步长
      *   而且定义了这三个选项必须定义numpartition
      *  6.numpartition：可用于表读写并行性的最大分区数如果写入时超过这个限制，可以使用coalesce（）重新分区
      *  7.queryTimeout：查询的超时时间，默认为0，不超时
      *  8.fetchsize：JDBC获取大小，它决定每次往返要获取多少行。
      *  9.batchsize：JDBC批处理大小，它决定每次往返插入多少行：默认值是1000。（仅适用于写操作）
      *  10.isolationLevel：事物的隔离级别：选项有：NONE, READ_COMMITTED, READ_UNCOMMITTED, REPEATABLE_READ, or SERIALIZABLE
      *  11.sessionInitStatement：在向远程数据库打开每个数据库会话之后，在开始读取数据之前，此选项执行一个自定义SQL语句(或PL/SQL块)。
      *      Example: option("sessionInitStatement", """BEGIN execute immediate 'alter session set "_serial_direct_read"=true'; END;""")
      *  12.truncate（默认为false）：当为true时，如果SaveMode为覆盖，则他导致Spark截断现有表，而不是删除并重新创建它。
      *  13.createTableOptions：如果指定，这个选项允许在创建表时设置特定于数据库的表和分区选项
      *      例：CREATE TABLE t (name string) ENGINE=InnoDB.)
      *  14.createTableColumnTypes：创建表时要使用的数据库列数据类型，而不是默认值。
      *      例：option("createTableColumnTypes", "name CHAR(64), comments VARCHAR(1024)")  （用于写操作）
      *  15.customSchema：用于从JDBC连接器读取数据的自定义模式（用于读操作），列名应和数据库的表的列表相同
      *      例：connectionProperties.put("customSchema", "id DECIMAL(38, 0), name STRING")
      *
      *  16.pushDownPredicate（默认是true）：启用或禁用下推到JDBC数据源的谓词的选项。
      *     在这种情况下，Spark将尽可能将过滤器下推到JDBC数据源。如果设置为false，所有的过滤器都将由Spark处理。
      *     当然如果是Spark比JDBC数据源更快地执行谓词过滤时，可以设置为false
      */


  }
}
