package com.zy.spark_test.spark_sql.data_source

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object source_parquet {
  def main(args: Array[String]): Unit = {
    /**
      * Parquet 是一种柱状格式，
      * SparkSQL支持读写Parquet file，这些文件自动保存原始数据的模式。
      * 在编写Parquet文件时，出于兼容性的原因，所有列都会自动转换为空。
      */
    val conf = new SparkConf()
    conf.setMaster("local[1]")
    val spark = SparkSession.builder()
      .appName("Spark SQL basic example")
      .config(conf)
      .getOrCreate()

    import spark.implicits._
    var peopleDF=spark.read
      .format("json")
      .load("examples/src/main/resources/people.json")

    peopleDF.write.parquet("people.parquet")

    var parquetFileDF: DataFrame = spark.read.parquet("people.parquet")

    parquetFileDF.createOrReplaceTempView("parquetFile")
    spark.sql("SELECT name FROM parquetFile WHERE age BETWEEN 13 AND 19")
      .show()


    //Partition Discovery分区的发现
    /**
      *及时在读取parquetFile时，其目录下有分区的设置（即像hive中的分区，不同的分区在不同的目录）
      * path
      * └── to
      * └── table
      * ├── gender=male
      * │   ├── ...
      * │   │
      * │   ├── country=US
      * │   │   └── data.parquet
      * │   ├── country=CN
      * │   │   └── data.parquet
      * │   └── ...
      * └── gender=female
      * ├── ...
      * │
      * ├── country=US
      * │   └── data.parquet
      * ├── country=CN
      * │   └── data.parquet
      * └── ...
      *
      * sparkSQL会自动的分区发现，并合并这些分区，数据读取之后将：
      * root
      * |-- name: string (nullable = true)
      * |-- age: long (nullable = true)
      * |-- gender: string (nullable = true)
      * |-- country: string (nullable = true)
      *
      * 自动类型推断：
      * spark.sql.sources.partitionColumnTypeInference.enabled 默认为true，开启
      */


    //Schema Merging
    /**
      * 这是一个耗时的操作，在spark1.5之后默认不开启：
      * spark.sql.parquet.mergeSchema = true 开启
      */
    //案例介绍
    var squaresDF: DataFrame = spark.sparkContext.makeRDD(1 to 5).map(i => (i, i * i))
      .toDF("value", "square")
    squaresDF.write.parquet("data/test_table/key=1")

    val cubesDF = spark.sparkContext.makeRDD(6 to 10).map(i => (i, i * i * i))
      .toDF("value", "cube")
    cubesDF.write.parquet("data/test_table/key=2")

    val mergedDF =spark.read.option("mergeSchema","true").parquet("data/test_table")
    mergedDF.printSchema()
    /**
      * root
      * |-- value: int (nullable = true)
      * |-- square: int (nullable = true)
      * |-- cube: int (nullable = true)
      * |-- key: int (nullable = true)
      * 实现了不同分区key，和字段的合并
      */

    //Configuration
    /**
      * spark.sqlContext.setConf()
      * spark.sql.parquet.binaryAsString（默认为false）：告诉Spark SQL将二进制数据解释为字符串，以提供与这些系统的兼容性
      * spark.sql.parquet.int96AsTimestamp（默认为true）：告诉Spark SQL将INT96数据解释为一个时间戳，以提供与这些系统的兼容性。
      * spark.sql.parquet.compression.codec（默认为 snappy）：设置数据的压缩格式：
      *   可接受的值包括:none、uncompression、snappy、gzip、lzo、brotli、lz4、zstd。
      *   注意，“zstd”要求在Hadoop 2.9.0之前安装“ZStandardCodec”，“brotli”要求安装“BrotliCodec”。
      * spark.sql.parquet.filterPushdown（默认为true）：过滤器下推优化
      * spark.sql.hive.convertMetastoreParquet（默认为true）：Spark SQL将使用Hive SerDe来代替内置的支持。内置的效果比较好
      * spark.sql.parquet.mergeSchema（默认为false）：开启元数据的合并
      * spark.sql.parquet.writeLegacyFormat默认为false）：fasle表示使用parquet新格式，为true表示使用Spark 1.4 以前的格式
      */
    spark.stop()
  }
}
