package com.zy.spark_test.spark_sql.data_source

import org.apache.spark.sql._
import java.io.File


object source_hive {
  def main(args: Array[String]): Unit = {
    /**
      * 使用spark加载hive的数据：
      *  1.由于Hive有大量依赖项，这些依赖项不包括在默认的Spark发行版中。如果可以在类路径上找到Hive依赖项，Spark将自动加载它们。
      *  2.Hive依赖项还必须出现在所有工作节点上，因为它们需要访问Hive序列化和反序列化库(SerDes)，以便访问存储在Hive中的数据。
      *  3.需要将：hive-site.xml, core-site.xml、hdfs-site.xml放入项目的conf下
      *  4.即使没有部署hive，spark也会自动在当前目录中创建metastore_db，并创建一个由spark.sql.warehouse配置的目录
      */
    /**
      * 操作hive需要添加hive的依赖：
      * <dependency>
      * <groupId>org.apache.spark</groupId>
      * <artifactId>spark-hive_2.11</artifactId>
      * <version>2.3.1</version>
      * </dependency>
      *
      */
    val warehouseLocation = new File("spark-warehouse").getAbsolutePath
    val spark = SparkSession.builder()
      .appName("source_hive")
      .config("spark.sql.warehouse.dir", warehouseLocation)
      .master("local[*]")
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._
    import spark.sql

    sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive")
    sql("LOAD DATA LOCAL INPATH 'file_collect//kv1.txt' INTO TABLE src")
    sql("SELECT * FROM src").show()
    sql("SELECT COUNT(*) FROM src").show()
    val sqlDF = sql("SELECT key, value FROM src WHERE key < 10 ORDER BY key")
    var stringDS: Dataset[String] = sqlDF.map {
      case Row(key: Int, value: String) => s"Key: $key, Value: $value"
    }
    stringDS.show()

    var RecordDF: DataFrame = spark.createDataFrame((1 to 100).map(i => Record(i, s"val_$i")))
    RecordDF.createOrReplaceTempView("records")
    sql("SELECT * FROM records r JOIN src s ON r.key = s.key").show()

    sql("CREATE TABLE hive_records(key int, value string) STORED AS PARQUET")

    val df = spark.table("src")
    df.write.mode(SaveMode.Overwrite).saveAsTable("hive_records")

    sql("SELECT * FROM hive_records").show()


    val dataDir = "file_collect//parquet_data"

    spark.range(10).write.parquet(dataDir)
    sql(s"CREATE EXTERNAL TABLE hive_ints(key int) STORED AS PARQUET LOCATION '$dataDir'")
    sql("SELECT * FROM hive_ints").show()

    //动态分区
    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    df.write.partitionBy("key").format("hive").saveAsTable("hive_part_tbl")
    sql("SELECT * FROM hive_part_tbl").show()
    spark.stop()

    /**
      * 注意：我们在创建表的时候，需要指定：format(“serde”, “input format”, “output format”)
      * 文件保存格式：USING hive OPTIONS(fileFormat 'parquet')
      * 这里fileFormat spark中支持6中：'sequencefile', 'rcfile', 'orc', 'parquet', 'textfile' and 'avro'
      *
      * inputFormat：org.apache.hadoop.hive.ql.io.orc.OrcInputFormat
      * OutputFormat ：org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat
      * 这两对必须成对出现，并且如果指定了fileFormat则无法指定它们。
      *
      *
      * serde：此选项指定serde类的名称当指定“fileFormat”选项时，如果给定的“fileFormat”已经包含serde的信息，则不要指定此选项。
      */
    /**
      * 不同版本hive的元数据
      * Spark SQL的Hive支持的最重要的部分之一是与Hive metastore的交互，这使得Spark SQL能够访问Hive表的元数据。
      * spark.sql.hive.metastore.version （默认为1.2.1）  hive的版本选择，可用选项从0.12.0到2.3.3
      * spark.sql.hive.metastore.jars （默认为builtin） 应该用于实例化hivemetoreclient的jar的位置：builtin、maven、classpath
      * spark.sql.hive.metastore.sharedPrefixes （默认为com.mysql.jdbc）
      * spark.sql.hive.metastore.barrierPrefixes (默认为empty)
      */
  }
}

case class Record(key: Int, value: String)