package com.zy.spark_test.spark_sql.data_source

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object source_json {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("datasource_spark")
    val spark = SparkSession.builder()
      .config(conf)
      .getOrCreate()

    /**
      * 注意点：
      *  1. Spark SQL可以自动推断JSON数据集的模式，并将其加载为数据集[行]。
      *  2. 这种转换可以在数据集[String]或JSON文件上使用sparkssession .read. JSON()完成
      *  3.作为json文件提供的文件不是典型的json文件。
      *  4.一行必须包含一个独立的、自包含的有效JSON对象。
      */
    val path = "file_collect//people.json"
    var peopleDF: DataFrame = spark.read.json(path)
    peopleDF.printSchema()

    peopleDF.createOrReplaceTempView("people")
    val teenagerNamesDF = spark.sql("SELECT name FROM people WHERE age BETWEEN 13 AND 19")
    teenagerNamesDF.show()

    spark.stop()

  }
}
