package com.zy.spark_test.spark_sql.data_source

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object datasource_spark {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("datasource_spark")
    val spark = SparkSession.builder()
      .config(conf)
      .getOrCreate()

    //1.默认的格式：spark.sql.sources.default=parquet
    var userDF: DataFrame = spark.read.load("examples/src/main/resources/users.parquet")
    userDF.select("name", "favorite_color").write.save("namesAndFavColors.parquet")

    //2.手动指定数据源(json、parquet、jdbc、orc、libsvm、csv、text)
    var peopleDF = spark.read.format("json").load("examples/src/main/resources/people.json")
    peopleDF.select("name", "age").write.format("parquet").save("namesAndAges.parquet")

    //3.加载CSV
    /**
      * sep:设置每个字段的分隔符
      * inferSchema: 默认为false，从数据自动推断输入模式。它需要额外传递一次数据。
      * header:默认为false，使用第一行作为列的名称。
      * 各参数解释见：DataFrameReader类
      */
    spark.read.format("csv")
      .option("sep", ";")
      .option("inferSchema", "true")
      .option("header", "true")
      .load("examples/src/main/resources/people.csv")

    //4.orc数据格式
    peopleDF.write.format("orc")
      .option("orc.bloom.filter.columns", "favorite_color")
      .option("orc.dictionary.key.threshold", "1.0")
      .save("users_with_options.orc")

    //5.直接使用sql查询文件
    var usersDF: DataFrame = spark.sql("select * from parquet.`examples/src/main/resources/users.parquet`")

    //6.saveMode
    /**
      * Append, 追加
      * Overwrite,覆盖
      * ErrorIfExists, 数据存在将报错
      * Ignore：如果数据已经存在将忽略本次save操作
      */
    usersDF.write.mode(SaveMode.Append).format("json")

    //7.Saving to Persistent Tables（hive的持久性表，默认的本地Hive metastore(使用Derby)）
    //Spark程序重新启动之后，只要您保持到相同亚稳态的连接，持久表仍然存在。

    //8.Bucketing, Sorting and Partitioning
    /**
      * 对于基于文件的数据源，还可以对输出进行存储、排序或分区。筛选和排序只适用于持久表:
      */
    //分桶表
    usersDF.write.bucketBy(42,"name").sortBy("age").saveAsTable("people_bucketed")
    //分区
    usersDF.write.partitionBy("favorite_color").format("parquet").save("namesPartByColor.parquet")

    //分区分桶表
    usersDF.write.partitionBy("favorite_color").bucketBy(4,"name")
      .saveAsTable("users_partitioned_bucketed")
  }
}
