package com.zy.spark_test.spark_sql.data_source


import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
object source_avro {
  def main(args: Array[String]): Unit = {
    /**
      * 自从Spark 2.4发布以来，Spark SQL为读取和写入Apache Avro数据提供了内置支持
      * 如果想使用交互式的命令或者spark-submit，那么spark-avro模块是外部的，默认情况下不包括在spark-submit或spark-shell中，
      * 必须手动设定：
      * ./bin/spark-submit --packages org.apache.spark:spark-avro_2.12:2.4.3 ...
      * ./bin/spark-shell --packages org.apache.spark:spark-avro_2.12:2.4.3 ...
      */
      val filename="file_collect//users.avro"
    val conf = new SparkConf().setMaster("local[2]").setAppName("datasource_spark")
    val spark = SparkSession.builder()
      .config(conf)
      .getOrCreate()
    //avro数据加载：由于这里avro sparkSQL中没有相应的API，这里只能使用format的方式
    //注意这里需要去官网下载相应的jar包导入项目com.databricks:spark-avro_2.11:4.0.0
    //网址：http://repo1.maven.org/maven2/com/databricks/spark-avro_2.11/4.0.0/spark-avro_2.11-4.0.0.jar
    var avroDF: DataFrame = spark.read.format("com.databricks.spark.avro").load(filename)
    avroDF.show()


    //to_avro() and from_avro()
    /**
      * Avro包提供函数to_avro以Avro格式将列编码为二进制，from_avro()将Avro二进制数据解码为列。
      * 当从Kafka这样的流媒体源读取或写入时，使用Avro记录作为列非常有用：
      *   1.可以使用from_avro()提取数据、充实数据、清理数据，然后再将其向下推到Kafka，或者将其写入文件
      *   2.将数据写入Kafka时将多个列重新编码为一个列时，此方法特别有用。
      * 当然这种方法仅限于java和Scala
      */
    /**
      * val jsonFormatSchema=new String(Files.readAllBytes(Paths.get("file_collect//user.avsc")))
      * val df = spark.readStream.format("kafka")
      *          .option("kafka.bootstrap.servers","host1:port1,host2:port2")
      *          .option("subscribe","topic1")
      *          .load()
      *
      * val output=df.select(from_avro('value, jsonFormatSchema as user'))
      * .where("user.favorite_color == \"red\"")
      * .select(to_avro($"user.name as value"))
      *
      * val query=output
      *           .writeStream
      *           .format("kafka")
      *           .option("kafka.bootstrap.servers","host1:port1,host2:port2")
      *           .option("topic","topic2")
      *           .start()
      */

     //.option() 参数介绍
    /**
      * avroSchema（默认为None）：用户以JSON格式提供的可选Avro模式。（生效于读和写）
      * recordName：写入结果中的顶级记录名
      * recordNamespace：在写入结果中记录名称空间
      * ignoreExtension：加载文件时可忽略文件的扩展名，加载所有文件(包含和不包含.avro扩展名)
      * compression（默认为snappy）：其他可选的有 uncompressed, snappy, deflate, bzip2 and xz.（写操作）
      *
      *
      */

    //SparkSession上的setConf方法：
    /**
      * spark.sql.legacy.replaceDatabricksSparkAvro.enabled（默认为true）:则数据源提供程序com.databricks.spark。为了向后兼容，avro被映射到内置但外部的avro数据源模块
      * spark.sql.avro.compression.codec（默认为snappy）：有option()中的compression选项相同
      * spark.sql.avro.deflate.level（默认为-1）：用于编写AVRO文件的压缩编解码器的压缩级别。有效值必须在1到9(含9)或-1之间。 -1表示6级
      *
      *
      */


  }
}
