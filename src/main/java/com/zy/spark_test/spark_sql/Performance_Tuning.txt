这里我们介绍关于spark-SQL的性能调节
（1）在内存中缓存数据
    spark-SQL通过：spark.catalog.cacheTable("tableName") ||dataFrame.cache()来使用内存中的列格式缓存表。
    然后spark只需扫描所需的列，并自动调优压缩，以减少内存使用和GC
    spark.catalog.uncacheTable("tableName") ||dataFrame..unpersist()  取消缓存
    SparkSession上的setConf 配置内存缓存：
        1.spark.sql.inMemoryColumnarStorage.compressed 启用压缩（默认为true）
        2.spark.sql.inMemoryColumnarStorage.batchSize (默认为10000) 控制用于列缓存的批的大小。防止OOM
（2）其他配置
    1.spark.sql.files.maxPartitionBytes（默认为128M）  读取文件时要装入单个分区的最大字节数
    2.spark.sql.files.openCostInBytes（默认为4 MB）    打开一个文件的估计成本
    3.spark.sql.broadcastTimeout （默认为300s）        广播连接中，等待的时间长度
    4.spark.sql.autoBroadcastJoinThreshold（默认为10 MB） 广播SQL，广播中的表数据的大小，-1表示禁用广播
    5.spark.sql.shuffle.partitions（默认为200）        为连接或聚合重组数据时要使用的分区数
（3）广播SQL查询提示
    广播提示引导Spark在将每个指定的表与另一个表或视图连接时广播它们。
    import org.apache.spark.sql.functions.broadcast
    broadcast(spark.table("src")).join(spark.table("records"), "key").show() 广播并连接两表