package com.zy.spark_test.spark_sql

import com.zy.spark_test.pojo.Score
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object RddToDataFrame {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setAppName("RddToDataFrame")
      .setMaster("local[*]")
    val spark= SparkSession.builder()
      .config(conf)
      .getOrCreate()
    import spark.implicits._

    val sc = spark.sparkContext
    var ScoreDF: DataFrame = sc.textFile("file_collect/scores.txt")
      .map(_.split("\\s+"))
      .map(attributes => Score(attributes(0), attributes(1).trim().toInt))
      .toDF()
    ScoreDF.createOrReplaceTempView("score")
    var sql=
      """
        |select t1.classid,
        |t1.score
        |from (
          |select
          |classid,
          |score,
          |row_number() over(PARTITION BY classid order by score desc ) as top
          |from score) as t1
        |where t1.top<=3
      """.stripMargin
    spark.sql(sql)
      .show()

    spark.stop()
  }
}
