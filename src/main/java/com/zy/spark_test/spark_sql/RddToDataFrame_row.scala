package com.zy.spark_test.spark_sql

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object RddToDataFrame_row {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setAppName("RddToDataFrame_row")
      .setMaster("local[*]")
    val spark= SparkSession.builder()
      .config(conf)
      .getOrCreate()
    var sc: SparkContext = spark.sparkContext
    import spark.implicits._

    var initRDD: RDD[String] = sc.textFile("file_collect//people.txt")
    val schemaString = "name  age"
    var fields=schemaString.split("\\s+")
      .map(fieldName=>StructField(fieldName,StringType,nullable = true))
    val schema=StructType(fields)

    val rowRDD =initRDD.map(_.split("\\s+"))
      .map(attributes => Row(attributes(0),attributes(1).trim))

    var people: DataFrame = spark.createDataFrame(rowRDD, schema)

    people.createOrReplaceTempView("people")

    spark.sql("SELECT * FROM people").show()
    spark.stop()
  }
}
