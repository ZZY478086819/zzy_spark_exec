package com.zy.spark_test.spark_sql

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.catalyst.plans.logical.Aggregate
import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}

import scala.collection.JavaConversions


case class Employee(name: String, salary: Long)
case class Average(var sum: Long, var count: Long)
case class Student(name:String, birthday:String, province:String)
object type_safe_user_defined extends Aggregator[Employee,Average,Double]{
  override def zero: Average = Average(0L, 0L)

  override def reduce(b: Average, a: Employee): Average = {
    b.sum+=a.salary
    b.count+=1
    b
  }

  override def merge(b1: Average, b2: Average): Average = {
    b1.sum+=b2.sum
    b1.count+=b2.count
    b1
  }

  override def finish(reduction: Average): Double = {
    reduction.sum.toDouble/reduction.count
  }

  override def bufferEncoder: Encoder[Average] = Encoders.product

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
object test_type_safe_user_defined{
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setMaster("local[1]")
    val spark= SparkSession.builder()
      .appName("Spark SQL basic example")
      .config(conf)
      .getOrCreate()
    var sc=spark.sparkContext
    //用于隐式转换，如将RDDs转换为数据流

    import spark.implicits._

    var ds: Dataset[Employee] = spark.read.json("file_collect//employees.json").as[Employee]
    ds.createOrReplaceTempView("employees")
    ds.show()
    print("**"*60)

    val averageSalary =type_safe_user_defined.toColumn.name("average_salary")
    ds.select(averageSalary).show()

    val stuList = List(
      new Student("委佩坤", "1998-11-11", "山西"),
      new Student("吴富豪", "1999-06-08", "河南"),
      new Student("戚长建", "2000-03-08", "山东"),
      new Student("王  伟", "1997-07-09", "安徽"),
      new Student("薛亚曦", "2002-08-09", "辽宁")
    )
    var rowList =stuList.map(stu => Row(stu.name, stu.birthday, stu.province))

    val schema=StructType(List(
      StructField("name", DataTypes.StringType, false),
      StructField("birthday", DataTypes.StringType, false),
      StructField("province", DataTypes.StringType, false)
    ))
    val javaList = JavaConversions.seqAsJavaList(rowList)
    var df: DataFrame = spark.createDataFrame(javaList, schema)
    df.show()

    //spark.createDataset(stuRDD).show()
    //spark.createDataset(stuList).show()

    spark.stop()
  }
}
