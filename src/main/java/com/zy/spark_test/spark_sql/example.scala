package com.zy.spark_test.spark_sql

import com.zy.spark_test.pojo.people
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object example {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setMaster("local[1]")
   val spark= SparkSession.builder()
      .appName("Spark SQL basic example")
        .config(conf)
          .getOrCreate()

    //用于隐式转换，如将RDDs转换为数据流
    import spark.implicits._
    var df: DataFrame = spark.read.json("D:\\workspace\\IDEA\\TestSpark\\file_collect\\people.json")
    //打印schema
    df.printSchema()
    /**
      * root
      * |-- age: long (nullable = true)
      * |-- id: long (nullable = true)
      * |-- name: string (nullable = true)
      *
      */
    /**
      * +---+---+-----+
      * |age| id| name|
      * +---+---+-----+
      * | 18|  1|  leo|
      * | 19|  2| jack|
      * | 17|  3|marry|
      * +---+---+-----+
      */
    df.show()

    //使用标准化的SQL
    df.createOrReplaceTempView("people")
    spark.sql("select * from people")
      .show()
    println("*"*60)
    //create dateset
    var peopleDS: Dataset[people] = spark.read.json("D:\\workspace\\IDEA\\TestSpark\\file_collect\\people.json").as[people]
    peopleDS.show()
    //3、关闭资源
    spark.stop()
  }
}
