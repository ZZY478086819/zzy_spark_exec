package com.zy.spark_test.spark_structured_streaming

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object Quick_Example {
  def main(args: Array[String]): Unit = {
    /**
      *结构化流中的关键思想是将实时数据流视为一个不断追加的表。导致了一个新的流处理模型，它非常类似于批处理模型。
      * 把流计算表示为标准的批处理式查询，就像在静态表上一样，Spark将它作为无界输入表上的增量查询运行
      */
    val conf=new SparkConf()
        .setMaster("local[2]")
      .setAppName("Quick_Example")
    val spark=SparkSession
        .builder()
        .config(conf)
      .getOrCreate()
    import spark.implicits._
    /**
      * readStream Input Sources （一些内置的数据源）：
      * 1.File source ：读取作为数据流写入目录中的文件，支持的文件格式有文本，csv, json, orc, parquet。
      *   path:文件路径
      *   maxFilesPerTrigger：是否先处理最新的新文件，当有大量积压文件时非常有用(默认值:false)
      *   fileNameOnly:是否仅根据文件名而不是完整路径检查新文件(默认值:false)
      * 2.Kafka source：从Kafka读取数据。
      *   kafka.bootstrap.servers:服务地址
      *   subscribe：topic
      * 3.Socket source（for testing）：从套接字连接读取UTF8文本数据。
      *   host：主机名
      *   port:端口
      * 4.Rate source (for testing) ：以每秒指定的行数生成数据，每个输出行包含一个时间戳和值。
      *   rowsPerSecond ：每秒应该生成多少行。
      *   rampUpTime 在生成速度变为rowsPerSecond之前，需要多长时间来提升。
      *   numPartitions ：生成行的分区号。
      *   案例：
      *  val userSchema = new StructType().add("name", "string").add("age", "integer")
      *   val csvDF = spark
      *     .readStream
      *     .option("sep", ";")
      *     .schema(userSchema)
      *     .csv("/path/to/directory")
      */
    //linesDF包含流文本数据的无界表，流文本数据的每一行成为表中的一行，（输入表）
    var linesDF: DataFrame = spark.readStream
      .format("socket")
      .option("host", "192.168.130.130")
      .option("port", "9999")
      .load()
    //使用.as[String]将数据DataFrame转换成为一个数据集合
    //flatMap，单词数据集包含生成的所有单词
    var wordsDS: Dataset[String] = linesDF.as[String].flatMap(_.split("\\s+"))
    //最后聚合，生成一个DataFrame，他表示一个流数据，（结束表）
    var wordsCount: DataFrame = wordsDS.groupBy("value").count()

    /**
      * 这里的format有三种模式：
      * Complete Mode：整个更新后的结果表将被写入外部存储，如何处理整个表的写入由存储连接器决定。
      * Append Mode：只将结果表中追加的新行，只适用于预期结果表中现有行不会更改的查询。
      * Update Mode：只将结果表中自最后一个触发器以来更新的行写入外部存储，如果查询不包含聚合，则等同于Append模式。
      */
    val query = wordsCount.writeStream
      //设置在每次更新的时候打印到控制台上
      .outputMode("complete")
      .format("console")
      .start()  //启动流计算
    query.awaitTermination()  //等待查询结束后终止
  }
}
