package com.zy.spark_test.spark_structured_streaming

import java.sql.Timestamp

import org.apache.spark.SparkConf
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._


object StructuredNetworkWordCountWindowed {
  def main(args: Array[String]): Unit = {
    if(args.length<3){
      System.err.println("Usage: StructuredNetworkWordCountWindowed <hostname> <port>" +
        " <window duration in seconds> [<slide duration in seconds>]")
      System.exit(1)
    }
    //主机，端口，窗口长度
    val Array(host,port,windowSize)=args
    //滑动长度
    val slideSize = if (args.length==3) windowSize else args(3).toInt

    val windowDuration= s"$windowSize seconds"
    val slideDuration=s"$slideSize seconds"

    val conf=new SparkConf().setAppName("StructuredNetworkWordCountWindowed")
      .setMaster("local[2]")
    val spark=SparkSession
      .builder()
      .config(conf)
      .getOrCreate()
    import spark.implicits._

    //输入源
    val lines=spark.readStream
      .format("socket")
      .option("host",host)
      .option("port",port.toInt)
      .option("includeTimestamp",true)
      .load()
    //转换为(时间戳，单词)
    var words: Dataset[(String, Timestamp)] = lines.as[(String, Timestamp)].flatMap(line => line._1.split("\\s+")
      .map(word => (word, line._2)))

    //Row： timestamp，word
    var windowedCounts: Dataset[Row] = words
        .withWatermark("timestamp","10 seconds") //“10秒”定义为允许数据延迟的阈值。
      .groupBy(window($"timestamp", windowDuration, slideDuration), $"word")
      .count().orderBy("window")
    val query=windowedCounts.writeStream
      .outputMode("complete")
      .format("console")
      .option("truncate","false")
      .start()

    query.awaitTermination()


  }
}
