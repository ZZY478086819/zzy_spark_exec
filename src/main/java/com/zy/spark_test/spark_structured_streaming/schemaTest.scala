package com.zy.spark_test.spark_structured_streaming

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.types.StructType


object schemaTest {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setMaster("local[2]")
      .setAppName("schemaTest")
    val spark = SparkSession
      .builder()
      .config(conf)
      .getOrCreate()
    import spark.implicits._

    val peopleSchema = new StructType().add("name", "string")
      .add("age", "string")
      .add("job", "string")

    var peopleDF: DataFrame = spark.readStream
      .option("sep", ";")
      .schema(peopleSchema)
      .csv("file_collect//csv")
    var peopleDS: Dataset[People] = peopleDF.as[People]
    peopleDS.printSchema()

    peopleDS.writeStream
      .format("parquet")
      .option("checkpointLocation", "file_collect//checkpiont//schemaTest_checkpoint")
      .option("path", "file_collect//parquet_data")
      .start()
      .awaitTermination()

  }
}

case class People(name: String, age: String, job: String)