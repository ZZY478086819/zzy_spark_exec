package com.zy.spark_test.spark_core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 对于多组数据，去每一组数据前N个数据，比如列出每个班级的前三名等等问题。
  * 解决的思路：先分组，然后每一组排序，取前N个。
  * 案例：有三个班级的分数清单scores.txt，取出每班前三名。
  */
object topN_Spark {
  def main(args: Array[String]): Unit = {
    var conf: SparkConf = new SparkConf().setAppName("topN_Spark").setMaster("local[1]")
    var sc: SparkContext = new SparkContext(conf)
    var initRDD: RDD[String] = sc.textFile("file_collect/scores.txt")
    var mapParRDD: RDD[(String, Int)] = initRDD.mapPartitions((it: Iterator[String]) => {
      var line_list: List[String] = it.toList
      var map_list: List[(String, Int)] = line_list.map(line => {
        var strings: Array[String] = line.split("\\s+")
        (strings(0), strings(1).toInt)
      })
      map_list.toIterator
    })
    //使用groupByKey 将相同班级的数据放在一个集合里
    mapParRDD.groupByKey().foreach(col=>{
      var key: String = col._1
      var values: Iterator[Int] = col._2.toIterator
      val arr=new Array[Int](3)
      while(values.hasNext){
        val score = values.next();
        val min=arr.min
        if (score>min){
          arr(arr.indexOf(min))=score
        }
      }
      println(key)
      arr.foreach(println)
    })
    sc.stop()
  }
}
