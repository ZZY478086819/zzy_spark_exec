package com.zy.spark_test.spark_core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object PV_Spark {
  def main(args: Array[String]): Unit = {
    /*
    *
    * PV是网站分析的一个术语，用以衡量网站用户访问的网页的数量。对于广告主，PV值可预期它可以带来多少广告收入。一般来说，PV与来访者的数量成正比，
    * 但是PV并不直接决定页面的真实来访者数量，如同一个来访者通过不断的刷新页面，也可以制造出非常高的PV。
    * */
    val conf=new SparkConf()
    conf.setMaster("local[2]").setAppName("PV_Spark")
    val sc=new SparkContext(conf)
    var fileRDD: RDD[String] = sc.textFile("file_collect/pv.log")

    //根据PV定义 某个页面/网址的访问数量  将每一条记录根据网址解析出一条访问量
    var URL_COUNT_RDD: RDD[(String, Int)] = fileRDD.mapPartitions((it: Iterator[String]) => {
      var rdd_list: List[String] = it.toList
      var colunm_count_list: List[(String, Int)] = rdd_list.map((line: String) => {
        val colunms: Array[String] = line.split("\\s+")
        val url = colunms(5)
        (url, 1)
      })
      colunm_count_list.toIterator
    })
    //累加页面访问量
    val reduceRDD = URL_COUNT_RDD.reduceByKey(_+_)
    var sortRDD: RDD[(String, Int)] = reduceRDD.sortBy(_._2, false)
    var result: Array[(String, Int)] = sortRDD.collect()
    result.foreach(println)
  }
}
