package com.zy.spark_test.spark_core

import com.zy.spark_test.pojo.SecondSortKey
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 二次排序对于两列以上的数据，要求对第一列排序之后，之后的列也要依次排序.
  * 思路就是：先对第一列进行排序，对于第一列数值相同，再对第二列进行排序。
  */
object second_sort_Spark {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[1]").setAppName("second_sort_Spark")
    val sc = new SparkContext(conf)
    var initRDD: RDD[String] = sc.textFile("file_collect/secondSort.txt")
    var mapParRDD: RDD[(SecondSortKey, String)] = initRDD.mapPartitions((it: Iterator[String]) => {
      var lineList: List[String] = it.toList
      var mapList: List[(SecondSortKey, String)] = lineList.map(line => {
        val columns: Array[String] = line.split("\\s+")
        val first: Int = columns(0).toInt
        val second: Int = columns(1).toInt
        var ss: SecondSortKey = new SecondSortKey(first, second)
        (ss, line)
      })
      mapList.toIterator
    })
    //sortByKey 会使用key也就是SecondSortKey的compareTo方法
    var resultRDD: RDD[(SecondSortKey, String)] = mapParRDD.sortByKey(false)
    resultRDD.map(obj=>{
      (obj._1.first,obj._1.second)
    }).saveAsTextFile("file_collect/secondSort_after")
      sc.stop()
  }
}
