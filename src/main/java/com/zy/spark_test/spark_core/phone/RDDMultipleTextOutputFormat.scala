package com.zy.spark.phone

import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat
;

/**
  * Created with IntelliJ IDEA.
  * User: ZZY
  * Date: 2019/8/12
  * Time: 11:23
  * Description:  实现spark多文件输出实例
  * 自定义多文件输出格式类，继承MultipleTextOutputFormat类，重写generateFileNameForKeyValue方法
  */
class RDDMultipleTextOutputFormat extends MultipleTextOutputFormat[Any, Any]{
  override def generateFileNameForKeyValue(key: Any, value: Any, name: String): String = {
    //联系人 + 电话号码 + 日期中的月份 组合成的作为 key
    //以电话号码作为文件名
    key.asInstanceOf[String].split("_")(1)+".txt"
  }
}