package com.zy.spark.phone

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
;

/**
  * Created with IntelliJ IDEA.
  * User: ZZY
  * Date: 2019/8/12
  * Time: 11:27
  * Description: 
  */
object PhoneCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    .setAppName("phone-count")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)
    var input="D:\\workspace\\IDEA\\SparkCoreOnYarn\\data\\phone.txt"
    var output="E:\\temp\\spark_test\\output\\phone_output"
    var inputRDD: RDD[String] = sc.textFile(input)
    var mapRDD : RDD[(String, Int)] = inputRDD.map(line => {
      val fields = line.split("\\s+")
      //1001    2001-5-1    18726234578 zs
      //联系人 + 电话号码 + 日期中的月份
      val id = fields(0)
      val hour = fields(1).split("-")(1)
      val phone = fields(2)
      val name = fields(3)
      (name + "_" + phone + "_" + hour, 1)
    })

    var resultRDD: RDD[(String, Int)] = mapRDD.reduceByKey(_ + _)
    resultRDD.saveAsHadoopFile(output,classOf[String], classOf[String],classOf[RDDMultipleTextOutputFormat])
  }
}