package com.zy.spark_test.spark_core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


object Initializing_Spark {
  def main(args: Array[String]): Unit = {
    val conf=new SparkConf()
    conf.setAppName("Initializing_Spark")
    conf.setMaster("local[1]")
    val context: SparkContext = new SparkContext(conf)
    val text_RDD: RDD[String] = context.textFile("file_collect/word.txt")
    var word_RDD: RDD[String] = text_RDD.flatMap(line => line.split("\\s+"))
    var word_count_RDD: RDD[(String, Int)] = word_RDD.map(word => (word, 1))
    var result: RDD[(String, Int)] = word_count_RDD.reduceByKey(_+_)
    result.foreach(rdd=>print(rdd._1,rdd._2))
    context.stop()
  }
}

