package com.zy.spark_test.spark_core

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object UV_Spark {
  def main(args: Array[String]): Unit = {
    /**
      * UV（unique visitor）即独立访客数，指访问某个站点或点击某个网页的不同IP地址的人数。在同一天内，UV只记录第一次进入网站的具有独立IP的访问者，在同一天内再次访问该网站则不计数。
      * UV提供了一定时间内不同观众数量的统计指标，而没有反应出网站的全面活动。
      */
    val conf = new SparkConf()
    conf.setAppName("UV_Spark")
    conf.setMaster("local[5]")
    var sc: SparkContext = new SparkContext(conf)
    val fileRDD = sc.textFile("file_collect/uv.log")
    var ip_url_count_distinct_rdd: RDD[(String, Int)] = fileRDD.mapPartitions((it: Iterator[String]) => {
      val line_list: List[String] = it.toList
      var tuples: List[(String, Int)] = line_list.map((line: String) => {
        var colunms: Array[String] = line.split("\\s+")
        (colunms(0) + "_" + colunms(5), 1)
      })
      tuples.toIterator
    }).distinct()
    var url_count_rdd: RDD[(String, Int)] = ip_url_count_distinct_rdd.map(line_count => {
      (line_count._1.split("_")(1), line_count._2)
    })
    var reduceRDD: RDD[(String, Int)] = url_count_rdd.reduceByKey(_ + _)
    var result: Array[(String, Int)] = reduceRDD.sortBy(_._2, false).collect()
    result.foreach(println)
  }
}
