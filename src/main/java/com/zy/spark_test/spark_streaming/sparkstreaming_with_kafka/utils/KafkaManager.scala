package com.zy.spark_test.spark_streaming.sparkstreaming_with_kafka.utils

import kafka.common.TopicAndPartition
import kafka.message.MessageAndMetadata
import kafka.serializer.StringDecoder
import org.apache.curator.framework.CuratorFramework
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.{KafkaUtils, OffsetRange}

object KafkaManager {
  //设置zookeeper中存放偏移量的位置
  val zkTopicOffsetPath="/offset"


  def ensureZKPathExists(zkPath: String, client: CuratorFramework) = {
    //如果为空的话就创建
    if(client.checkExists().forPath(zkPath)==null){
      //如果父目录不存在，连父目录一起创建
      client.create().creatingParentsIfNeeded().forPath(zkPath)
    }
  }

  /**
    * 获取对应的topic中的每一个partition的偏移量信息
    *
    * @param topic
    * @param group
    * @param client
    * @return
    */
  def getFromOffsets(topic: String, group: String, client: CuratorFramework):(Map[TopicAndPartition, Long], Boolean) = {
    //构建存储offset的位置信息的路径
    val zkPath=s"${zkTopicOffsetPath}/${topic}/${group}"
    //判断当前路径是否存在，不存在则创建
    ensureZKPathExists(zkPath,client)

    //获取所有分区中，存储的offset信息
    import scala.collection.JavaConversions._
    val offsets=for{p<-client.getChildren.forPath(s"${zkPath}")}yield {
      val offset=new String(client.getData.forPath(s"$zkPath/${p}")).toLong
      (new TopicAndPartition(topic,p.toInt),offset)
    }
    //如果未空表示第一次读取,无偏移量信息
    if(offsets.isEmpty){
      (offsets.toMap,false)
    }else{
      (offsets.toMap,true)
    }
  }

  /**
    * 创建kafka对应的message（分两种情况）：
    *   1.第一次消费的时候，从zk中读取不到偏移量
    *   2.之后的消费从zk中才能读取到偏移量
    *
    * @param topic 获取kafka的主题
    * @param group 获取kafka的组
    * @param ssc StreamingContext对象
    * @param kafkaParams kafka节点信息
    * @param client 获取一个操作zookeeper的对象
    */
  def createMessage(topic:String,
                    group:String
                    ,ssc:StreamingContext
                    ,kafkaParams:Map[String,String]
                    ,client:CuratorFramework):DStream[(String,String)]={
    //获取偏移量，以及判断是否是第一次消费
    val (fromOffsets,flag)=getFromOffsets(topic, group,client)
    var message:DStream[(String,String)]=null

    //构建kafka对应的message
    if(flag){ //标记位，使用zk中得到的对应的partition偏移量信息，如果有为true
      val messageHandler=(mmd: MessageAndMetadata[String, String]) => (mmd.key, mmd.message)
      //从设置的zk中读取数据
      message = KafkaUtils.createDirectStream[String,
        String,
        StringDecoder,
        StringDecoder,
        (String,String)](ssc,kafkaParams,fromOffsets,messageHandler)
    }else{ //第一次读取
      message=KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
        ssc,
        kafkaParams,
        topic.split(",").toSet
      )
    }
    message
  }

  /**
    * 将最新的偏移量，更新到对应的分区中去
    * OffsetRange就代表了读取到的偏移量的数据范围
    */
  def storeOffsets(offsetRanges:Array[OffsetRange],group:String,client:CuratorFramework)={
    for(offsetRange<- offsetRanges){
      //获取分区
      val partiton=offsetRange.partition
      //获取主题
      val topic = offsetRange.topic
      //获取偏移量
      val offset=offsetRange.untilOffset

      //构建存放偏移量的znode
      val path=s"${zkTopicOffsetPath}/${topic}/${group}/${partiton}"
      //判断是否存在，不存在则创建
      ensureZKPathExists(path,client)
      //更新偏移量
      client.setData().forPath(path,(offset+"").getBytes())
    }
  }
}
