package com.zy.spark_test.spark_streaming.sparkstreaming_with_kafka

import java.util.Properties

import com.zy.spark_test.spark_streaming.sparkstreaming_with_kafka.utils.KafkaManager
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka.HasOffsetRanges
import org.apache.spark.streaming.{Seconds, StreamingContext}

object SparkStreamingKafka2Kafka {
  //存储读取kafka偏移量的根路径
  val zkTopicOffsetPath="/offsets"

  //操作zookeeper对象
  val client={
    val client=CuratorFrameworkFactory.builder()
      .connectString("hadoop01:2181,hadoop02:2181,hadoop03:2181/kafka")
      .retryPolicy(new ExponentialBackoffRetry(3000,3))
      .build()
    client.start()
    client
  }
  def main(args: Array[String]): Unit = {
    if (args==null||args.length<4){
      """
        |Parameter Errors! Usage: <batchInterval> <groupId> <topics>
        |batchInterval        : 批次间隔时间
        |groupId              : 消费组的id
        |src-topic            : 读取的topic
        |dest-topic           : 写入的topic
      """.stripMargin

      System.exit(1)
    }
    val Array(batchInterval,groupID,src_topic,dest_topic)=args
    val conf=new SparkConf()
      .setMaster("local[2]")
      .setAppName("SparkStreamingKafka2Kafka")
    val kafkaParams=Map(
      "bootstrap.servers"->"hadoop01:9092,hadoop02:9092,hadoop03:9092",
      "auto.offset.reset"->"smallest"
    )
    val ssc=new StreamingContext(conf,Seconds(batchInterval.toLong))
    val message=KafkaManager.createMessage(src_topic,groupID,ssc,kafkaParams,client)
    message.foreachRDD(rdd=>{
      if(!rdd.isEmpty()){
        val cleanRDD=rdd.map({case(key,msg)=>{
          val fields=msg.split("<<<!>>>,<<<!>>>")
          if(fields==null||fields.length<15) {
            ""
          }else{
            val field1 = fields(0).replaceAll("<<<!>>>", "")
            val field2 = fields(1)
            val dateTime = fields(2)
            val srcIpPort = fields(3) + ":" + fields(5)
            val destIpPort = fields(4) + ":" + fields(6)
            val url = fields(13)
            val info = fields(14).replaceAll("<<<!>>>", "")
            val result = s"${field1}|${field2}|${dateTime}|${srcIpPort}|${destIpPort}|${url}|${info}"
            result
          }
        }})
        //更新偏移量
        KafkaManager.storeOffsets(rdd.asInstanceOf[HasOffsetRanges].offsetRanges,groupID,client)
      }
    })
    ssc.start()
    ssc.awaitTermination()
  }
}
