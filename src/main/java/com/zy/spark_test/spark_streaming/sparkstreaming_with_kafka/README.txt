sparkStreaming整合kafka：
（1）部署条件
 	→安装 好zookeeper和kafka和flume
 	→启动zookeeper和kafka
 	→Hadoop 的高可用
（2）创建kafkatopic
    #查看kafka topic
        kafka-topics.sh \
        --list \
        --zookeeper hadoop01:2181,hadoop02:2181,hadoop03:2181 \
    #创建kafka topic
        kafka-topics.sh \
        --create \
        --zookeeper hadoop01:2181,hadoop02:2181,hadoop03:2181 \
        --replication-factor 3 \
        --partitions 3 \
        --topic flume-kafka
    #显示一个 kafka topic 的详细信息
        kafka-topics.sh \
        --zookeeper hadoop01:2181,hadoop02:2181,hadoop03:2181 \
        --describe \
        --topic flume-kafka
（3）flume脚本：
        source（dir） --- > channel（memory）  --- > sink （kafka）

（4）sparkStreaming接受kafka数据：
    1）receiver +checkpoint方式
        1. 	kafka中的topic和sparkstreaming中生成的RDD分区没有关系，在KafkaUtils.createStream中增加分区数只会增加单个receiver的线程数，不会增加spark的并行度
        2. 	可以创建多个kafka的输入DStream，使用不同的group和topic，使用多个receiver并行接收数据
        3. 	如果启用了HDFS等有容错的存储系统，并且启用了写入日，则接收到的数据已经被复制到日志中。
    2）direct +zookeeper方式
        1. 	不需要创建多个输入kafka流并将其合并，使用directStream，spark Streaming将创建于使用kafka分区一样多的RDD分区，这些分区的数据全部从kafka并行读取数据，kafka和RDD分区之间有一对一的映射关系
        2. 	Direct方式没有接收器，不需要预先写入日志，只要kafka数据保留时间足够长就行保证了正好一次的消费语义

这里我们编写案例使用direct +zookeeper方式将kafka的数据加载到sparkStreaming中


