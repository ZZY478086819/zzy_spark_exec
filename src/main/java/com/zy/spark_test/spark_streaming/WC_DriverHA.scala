package com.zy.spark_test.spark_streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

object WC_DriverHA {
  val checkpoint_dir="file_collect\\checkpoint\\WC_DriverHA_checkpoint"
  def main(args: Array[String]): Unit = {
    /**
      * 如果程序异常退出后，重启程序，之前的计算将重新开始，
      * 因为编程入口StreamingContext在代码重新运行的时候，是重新生成的，为了使程序在异常退出的时候，在下次启动的时候，依然可以获得上一次的StreamingContext对象，保证计算数据不丢失，此时就需要将StreamingContext对象存储在持久化的系统中。
      * 也就是说需要制作StreamingContext对象的HA。
      *
      * 注意：正常情况下，使用这种方式的HA，只能持久状态数据到持久化的文件中，
      * 默认情况是不会持久化StreamingContext对象到CheckPointDriectory中的。
      */


    /**
      * StreamingContext.getOrCreate()
      * 第一个参数：checkpointPath用于存储StreamingContext对象:
      *   如果是第一次执行，那么在在这个checkpointDriectory目录中是不存在streamingContext对象的，所以要创建
      *   如果是第二个运行，那么这个checkpointDriectory目录中就会已存在存储了第一次运行时生成的StreamingContext对象,
      *   第二次运行的时候，就不会在创建，则是从checkpointDriectory目录中读取进行恢复。
      * 第二个参数：creatingFunc: () => StreamingContext：用于创建StreamingContext对象
      */
    val ssc=StreamingContext.getActiveOrCreate(checkpoint_dir,functionToCreateContext)
    //仍然需要手动启动
    ssc.start()
    ssc.awaitTermination()
    ssc.stop()
  }
  def functionToCreateContext():StreamingContext={
    val conf = new SparkConf().setAppName("WC_DriverHA").setMaster("local[2]")
    val ssc = new StreamingContext(conf,Seconds(2))
    val inputDStream = ssc.socketTextStream("test",9999)

    //获取中间状态的存放目录，此时上一次的streamingContext对象的状态信息，也会保存在其中
    ssc.checkpoint(checkpoint_dir)

    val wordDStream = inputDStream.flatMap(_.split("\\s+")).map(kv=>(kv,1))

    var word_count_DStream: DStream[(String, Int)] = wordDStream.updateStateByKey(updateFunc)

    word_count_DStream.print()

    //提交sparkStreaming应用程序
    ssc.start()
    ssc.awaitTermination()

    ssc
  }
  def updateFunc(values:Seq[Int],status:Option[Int]):Option[Int]={
    val s1=values.sum
    val s2=status.getOrElse(0)
    Some(s1+s2)
  }
}
