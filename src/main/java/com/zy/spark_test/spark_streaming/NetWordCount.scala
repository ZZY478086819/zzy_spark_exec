package com.zy.spark_test.spark_streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object NetWordCount {
  def main(args: Array[String]): Unit = {
    //1.获取编程入口，StreamingContext
    val conf = new SparkConf().setAppName("NetWordCount")
      .setMaster("local[2]")
    val ssc=new StreamingContext(conf,Seconds(2))

    //2.获取inputDStream
    var inputDStream: ReceiverInputDStream[String] = ssc.socketTextStream("test", 9999)

    //3.对DStream进行各种的transformation操作
    var wordDStream=inputDStream.flatMap(_.split("\\s+")).map(kv=>(kv,1))
    var word_count_DStream: DStream[(String, Int)] = wordDStream.reduceByKey(_ + _)

    //4.对于数据结果进行output操作
    word_count_DStream.print()

    //5.提交sparkStreaming应用程序
    ssc.start()
    ssc.awaitTermination()
    ssc.stop()
  }
}
