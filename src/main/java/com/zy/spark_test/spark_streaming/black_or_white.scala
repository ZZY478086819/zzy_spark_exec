package com.zy.spark_test.spark_streaming

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * 使用transform算子，实现将黑名单中的数据从DStream中过滤
  */
object black_or_white {
  def main(args: Array[String]): Unit = {
    val checkpoint_dir="file_collect\\checkpoint\\black_or_white_checkpoint"
    //定义黑名单
    val black_list = List("@", "#", "$", "%")

    val conf = new SparkConf()
      .setMaster("local[2]")
      .setAppName("black_or_white")
    val ssc=new StreamingContext(conf,Seconds(2))

    var inputDStream: ReceiverInputDStream[String] = ssc.socketTextStream("test", 9999)
    //将黑名单广播出去
    val bc=ssc.sparkContext.broadcast(black_list)

    ssc.checkpoint(checkpoint_dir)

    var wordDStream: DStream[String] = inputDStream.flatMap(_.split("\\s+"))

    //过滤数据
    /**
      * 定义transform中实现的逻辑:
      * 这个方法表示：从DStream拿出来一个RDD，经过transformsFunc计算之后，返回一个新的RDD
      */
    def transformsFunc(rdd:RDD[String]):RDD[String] ={
      //将rdd切分出每一个分区，处理分区中的数据
      var newRDD: RDD[String] = rdd.mapPartitions(ptndata => {
        val blackList: List[String] = bc.value
        ptndata.filter(data => !blackList.contains(data))
      })
      newRDD
    }

    val fileterdDStream=wordDStream.transform(transformsFunc _)

    //统计相应的单词数
    var tuplewordDStream: DStream[(String, Int)] = fileterdDStream.map((_, 1))

    var resultDStream: DStream[(String, Int)] = tuplewordDStream.updateStateByKey((seq: Seq[Int], stat: Option[Int]) => {
      Option(seq.sum + stat.getOrElse(0))
    })

    resultDStream.print()

    ssc.start()
    ssc.awaitTermination()

    ssc.stop()

  }
}
