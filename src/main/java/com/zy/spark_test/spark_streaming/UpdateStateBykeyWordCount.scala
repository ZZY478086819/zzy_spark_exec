package com.zy.spark_test.spark_streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object UpdateStateBykeyWordCount {
  def main(args: Array[String]): Unit = {
    //到现在为止，统计过去时间段内的所有key对应的状态。

    val checkpoint_dir="file_collect\\checkpoint\\UpdateStateBykeyWordCount_checkpoint"

    val conf=new SparkConf().setMaster("local[2]")
      .setAppName("UpdateStateBykeyWordCount")

    val ssc=new StreamingContext(conf,Seconds(2))

    var inputDStream: ReceiverInputDStream[String] = ssc.socketTextStream("test", 9999)

    //对当前程序，设置一个检查目录，用于存放之前状态的中间值。
    ssc.checkpoint(checkpoint_dir)

    var wordDStream: DStream[(String, Int)] = inputDStream.flatMap(_.split("\\s+")).map(kv => (kv, 1))

    /**
      * updateStateByKey是状态更新函数，
      * updateFunc: (Seq[V], Option[S]) => Option[S]
      * (U,C)=>C
      */
    var word_count_DStream: DStream[(String, Int)] = wordDStream.updateStateByKey(updateFunc)

    word_count_DStream.print()

    ssc.start()

    ssc.awaitTermination()

    ssc.stop()
  }

  /**
    *values:Seq[Int],state:Option[Int]==>Option[Int]
    * @param values :新值
    * @param state ：状态值
    * @return
    *
    */
  def updateFunc(values:Seq[Int],state:Option[Int]):Option[Int]={
    //当前时间片中的word的次数
    val new_value:Int=values.sum
    val stat_vale: Int=state.getOrElse(0)
    Some(new_value+stat_vale)
  }
}
