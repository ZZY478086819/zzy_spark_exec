package com.zy.spark_streaming;

import org.apache.spark.streaming.{Seconds, StreamingContext};
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream};
import org.apache.spark.streaming.flume.{FlumeUtils, SparkFlumeEvent};
import org.apache.spark.{SparkConf, SparkContext};


/**
  * Created with IntelliJ IDEA.
  * User: ZZY
  * Date: 2019/8/13
  * Time: 10:56
  * Description: 
  */

object SparkStreamingFlume_Push {
  def main(args: Array[String]): Unit = {
    //1、创建sparkConf
    val sparkConf: SparkConf = new SparkConf()
      .setAppName("SparkStreamingFlume_Push")
      .setMaster("local[2]")
    //2、创建sparkContext
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    //3、创建StreamingContext
    val ssc = new StreamingContext(sc, Seconds(5))
    //4、获取flume中的数据
    val stream: ReceiverInputDStream[SparkFlumeEvent] = FlumeUtils.createStream(ssc, "localhost", 1111)
    //5、从Dstream中获取flume中的数据  {"header":xxxxx   "body":xxxxxx}
    val lineDstream: DStream[String] = stream.map(x => new String(x.event.getBody.array()))
    //6、切分每一行,每个单词计为1
    val wordAndOne: DStream[(String, Int)] = lineDstream.flatMap(_.split(" ")).map((_, 1))
    //7、相同单词出现的次数累加
    val result: DStream[(String, Int)] = wordAndOne.reduceByKey(_ + _)
    //8、打印输出
    result.print()
    //开启计算
    ssc.start()
    ssc.awaitTermination()
  }
}