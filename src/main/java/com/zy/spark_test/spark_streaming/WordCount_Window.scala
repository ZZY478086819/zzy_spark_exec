package com.zy.spark_test.spark_streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

object WordCount_Window {
  def main(args: Array[String]): Unit = {
    /**
      * 在做window操作时
      * 	窗口覆盖的数据流的时间长度，必须是批处理时间间隔的倍数。
      * 	前一个窗口到后一个窗口所经过的时间长度，必须是批处理时间间隔的倍数。
      *
      */
    //参数检查
    if (args==null||args.length<4){
      println(
        """
          |Parameter Errors! Usage: <hostname> <port> <batchInvail> <checkpoint>
          |hostname     : 主机名
          |port         : 端口
          |batchInvail  : 批处理时间
          |checkpoint   : 检查点目录
        """.stripMargin)
      System.exit(1)
    }

    val Array(hostname,port,batchInvail,checkpoint)=args

    val conf = new SparkConf().setMaster("local[2]").setAppName("WordCount_Window")
    val ssc=new StreamingContext(conf,Seconds(batchInvail.toLong))

    var inputDStream: ReceiverInputDStream[String] = ssc.socketTextStream(hostname, port.toInt)


    val tupleDStream = inputDStream.flatMap(_.split("\\s+")).map((_,1))


    //窗口操作
    /**
      * 每隔4秒，算过去6秒的数据
      * reduceFunc:数据合并的函数
      * windowDuration：窗口的大小（过去6秒的数据）
      * slideDuration：窗口滑动的时间（每隔4秒）
      */
    val resultDStream = tupleDStream.reduceByKeyAndWindow((kv1: Int, kv2: Int) => kv1 + kv2,
      Seconds(batchInvail.toLong * 3),
      Seconds(batchInvail.toLong * 2))

    resultDStream.print()

    ssc.start()

    ssc.awaitTermination()

    ssc.stop()
  }
}
