package com.zy.spark_test.spark_streaming

import java.util.Properties

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

object StreamingForeachRDD {
  def main(args: Array[String]): Unit = {
    /**
      * 假如想将流数据数据写入MySQL：
      * ForeachRDD是不错的选择
      */

    val conf=new SparkConf().setAppName("StreamingForeachRDD")
      .setMaster("local[2]")
    val ssc=new StreamingContext(conf,Seconds(2))
    val spark=SparkSession.builder()
      .config(conf)
      .getOrCreate()
    import spark.implicits._
    val initDStream = ssc.socketTextStream("test",9999)

    var word_countDStream=initDStream.flatMap(_.split("\\s+")).map((_,1))

    var resultDStream: DStream[(String, Int)] = word_countDStream.reduceByKeyAndWindow((kv1: Int, kv2: Int) => kv1 + kv2,
      Seconds(10),
      Seconds(4))

    resultDStream.foreachRDD(rdd=>{
      var WordsRDD: RDD[Words] = rdd.map(wc => new Words(wc._1, wc._2))
      var rddDS: Dataset[Words] = spark.createDataset(WordsRDD)
      val url="jdbc:mysql://localhost:3306/test"
      val table_name="wodcount"
      val prots=new Properties()
      prots.put("user","root")
      prots.put("password","123456")
      rddDS.write
          .mode(SaveMode.Append)
        .jdbc(url,table_name,prots)
    })

    ssc.start()
    ssc.awaitTermination()

    ssc.stop()
  }
}
case class Words(word:String,count:Int)