package com.zy.spark_streaming;

import java.net.InetSocketAddress;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream};
import org.apache.spark.streaming.flume.{FlumeUtils, SparkFlumeEvent};
import org.apache.spark.streaming.{Seconds, StreamingContext};
import org.apache.spark.{SparkConf, SparkContext};


/**
  * Created with IntelliJ IDEA.
  * User: ZZY
  * Date: 2019/8/13
  * Time: 9:47
  * Description: 
  */
object SparkStreamingFlume_Poll {
  def main(args: Array[String]): Unit = {
    //1、创建sparkConf
    val conf=new SparkConf()
      .setAppName("SparkStreamingFlume_Poll")
      .setMaster("local[2]")
    //2、创建sparkContext
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")
    //3、创建StreamingContext
    val ssc=new StreamingContext(sc,Seconds(3))
    //4、获取flume中数据
    var FlumeStream: ReceiverInputDStream[SparkFlumeEvent] = FlumeUtils.createPollingStream(ssc, "hadoop03",8899, StorageLevel.MEMORY_ONLY)
    //5、从Dstream中获取flume中的数据  {"header":xxxxx   "body":xxxxxx}
    var lineDS: DStream[String] = FlumeStream.map(x => {
      new String(x.event.getBody.array())
    })
    //6、切分每一行,每个单词计为1
    var wordDS: DStream[(String, Int)] = lineDS.flatMap(_.split("\\s+")).map((_, 1))
    //7、相同单词出现的次数累加
    val result: DStream[(String, Int)] =wordDS.reduceByKey(_+_)
    //8、打印输出
    result.print()
    //9.开启计算
    ssc.start()
    ssc.awaitTermination()
    ssc.stop()
  }
}