package com.zy.spark_test.pojo

import java.io.{DataInput, DataOutput}

import org.apache.hadoop.io.WritableComparable

case class SecondSortKey(var first:Int,var second:Int) extends WritableComparable[SecondSortKey]{

  /**
    * 重写无参构造函数,用于反序列化时的反射操作
    */
  def this(){
    this(0, 0)
  }

  override def write(out: DataOutput): Unit = {
    out.writeInt(this.first)
    out.writeInt(this.second)
  }

  override def readFields(in: DataInput): Unit = {
    this.first=in.readInt()
    this.second=in.readInt()
  }

  /**
    * 继承Comparable接口需要实现的方法,用于比较两个对象的大小
    * @param o
    * @return
    */
  override def compareTo(o: SecondSortKey): Int = {
    if (this.first-o.first==0){
      return this.second-o.second
    }
    return this.first-o.first
  }
}
